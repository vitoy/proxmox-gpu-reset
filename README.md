# Reset Gpu that you want to pass to a VM with hookscript on Proxmox 7.2 (5.15.39-2-pve)


## The script is ran before the VM is started, reseting the GPU, and properly passing to the VM.


## First find the GPU adress with `lspci`. Should look something like this : 0c:00.0 .

```console

root@pve:~# lspci
...

0c:00.0 VGA compatible controller: Advanced Micro Devices, Inc. [AMD/ATI] Ellesmere [Radeon RX 470/480/570/570X/580/580X/590] (rev e7)
0c:00.1 Audio device: Advanced Micro Devices, Inc. [AMD/ATI] Ellesmere HDMI Audio [Radeon RX 470/480 / 570/580/590]
0d:00.0 VGA compatible controller: Advanced Micro Devices, Inc. [AMD/ATI] RV635 [Radeon HD 3650/3750/4570/4580]
0d:00.1 Audio device: Advanced Micro Devices, Inc. [AMD/ATI] RV635 HDMI Audio [Radeon HD 3650/3730/3750]

...

```

## Copy the GPU adress and replace it with yours in this command 
  `echo 1 > /sys/bus/pci/devices/0000\:0c\:00.0/remove`


## Actual script

``` bash

#!/bin/bash

if [ $2 == "pre-start" ]
then
    echo "gpu-hookscript: Resetting GPU for Virtual Machine $1"
    echo 1 > /sys/bus/pci/devices/0000\:0c\:00.0/remove
    echo 1 > /sys/bus/pci/rescan
fi

```


## How to make it work

### find you VM id and replace it in the last command

```bash

#create snippets folder
mkdir /var/lib/vz/snippets

#create script with content above
nano /var/lib/vz/snippets/gpu-hookscript.sh

#make it executable
chmod +x /var/lib/vz/snippets/gpu-hookscript.sh

#apply script to VM
qm set 100 --hookscript local:snippets/gpu-hookscript.sh

```

### Thanks @nick.kopas and @StephenM64 from the Proxmox Community for providing this solution I got my GPU working with this method. [Forum Post](https://forum.proxmox.com/threads/gpu-passthrough-issues-after-upgrade-to-7-2.109051/post-469855)